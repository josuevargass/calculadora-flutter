import 'package:calculadora/screens/calculator_controller.dart';
import 'package:calculadora/widgets/sub_result.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import 'line_separator.dart';
import 'main_result.dart';

class MathResults extends StatelessWidget {
  final calculatorCtrl = Get.find<CalculatorController>();

  @override
  Widget build(BuildContext context) {
    return Obx(() => Column(
          children: [
            SubResult(text: calculatorCtrl.firstNumber.toString()),
            SubResult(text: calculatorCtrl.operation.toString()),
            SubResult(text: calculatorCtrl.secondNumber.toString()),
            LineSeparator(),
            MainResultText(text: calculatorCtrl.mathResult.toString()),
          ],
        ));
  }
}
